#include <stdio.h>
#include <string.h>
#include <Windows.h>

void main()
{
	//CSVファイルから読み込んだ値をこの配列に入れたい
	int table[6];	



	//********************************ここからはファイルを開いて、中身をchar型の変数に入れる処理********************************
	//ファイルハンドルを入れる変数
	HANDLE hFile;

	//ファイルを開く	↓ファイル名	↓「開く」の意味		↓「存在するファイルしか開かない」の意味
	hFile = CreateFile("TestData.csv", GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	//開けなかったら中断
	if (hFile == NULL)
		return;

	//ファイルのサイズ（文字数）を取得
	DWORD fileSize = GetFileSize(hFile, NULL);

	//ファイルのサイズ分配列を確保
	char* data;
	data = new char[fileSize];

	//ファイルの中身を上記の変数に全部コピー
	DWORD dwBytes = 0;
	ReadFile(hFile, data, fileSize, &dwBytes, NULL);

	//ファイルを閉じる
	CloseHandle(hFile);



	//********************************ここからはchar型の変数に入った文字列を「,」で区切ってバラバラのデータにする処理********************************
	//まず区切り文字を決める
	char separate[] = ",\n\r";	//「,」と「\n\r」を区切り文字とする（\n\rは改行の意味）

	//最初の区切り文字までの文字列を取得
	char* tok = strtok(data, separate);		//strtokは文字列を分割する関数。第一引数が元データ、第二引数が区切り文字。

	//データが無くなるまで繰り返す
	int i = 0;
	while (tok != NULL)			//データを全部バラバラにするとstrtokの戻り値はNULLになる
	{
		//取得した文字列を数値に変換して配列へ
		table[i] = atoi(tok);	//atoiは文字列を数値に変換する関数
		i++;

		//２回目以降
		tok = strtok(NULL, separate);	//第一引数をNULLにすると、前回の続きからデータをバラしていく
	}



	//********************************ここからは確認のために、配列の中身を表示する処理********************************
	for (int i = 0; i < 6; i++)
	{
		printf("%d\n", table[i]);
	}

}